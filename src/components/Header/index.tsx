import React from 'react';
import {View, Text} from 'react-native';

export const Header = () => {
  return (
    <View
      style={{
        borderRadius: 10,
        backgroundColor: '#24292E',
        marginHorizontal: 20,
        height: 100,
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text style={{color: '#fff'}}>Welcome to Print App</Text>
      <Text style={{color: '#fff'}}>Please Choose Device to Print </Text>
    </View>
  );
};
