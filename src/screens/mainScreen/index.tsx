import React from 'react';
import {Header} from '../../components';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';
import {TouchableOpacity, Text, Button} from 'react-native';
import Toast from 'react-native-toast-message';

export const MainScreen = () => {
  const [printers, setPrinters] = React.useState<any>([]);
  const [currentPrinter, setCurrentPrinter] = React.useState<any>();

  React.useEffect(() => {
    BLEPrinter.init()
      .then(() => {
        BLEPrinter.getDeviceList()
          .then(setPrinters)
          .catch(e => console.log('e', e));
      })
      .catch(e =>
        Toast.show({
          type: 'error',
          text1: e,
          text2: 'please enable bluetooth',
        }),
      );
  }, []);

  const _connectPrinter = (printer: any) => {
    //connect printer
    BLEPrinter.connectPrinter(printer.inner_mac_address).then(
      setCurrentPrinter,
      error => console.warn(error),
    );
  };

  const printTextTest = () => {
    if (currentPrinter === undefined) {
      Toast.show({
        type: 'error',
        text1: 'please choose device',
      });
    } else {
      currentPrinter && BLEPrinter.printText('<C>hello doctor manal</C>\n');
    }
  };
  return (
    <>
      <Header />
      {printers.map((printer: any) => (
        <TouchableOpacity
          style={{marginVertical: 20, alignItems: 'center'}}
          key={printer.inner_mac_address}
          onPress={() => _connectPrinter(printer)}>
          <Text>{printer.device_name}</Text>
        </TouchableOpacity>
      ))}
      <Button color="#24292E" title="test" onPress={printTextTest} />
    </>
  );
};
